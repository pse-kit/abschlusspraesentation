# Abschlusspräsentation
Abschlusspräsentation des PSE-Projekts "Webservice zur Definition und Durchsetzung des Mehr-Augen-Prinzips" im WS2018/19 von Julius Häcker, Moritz Leitner, Nicolas Schuler, Noah Wahl und Wendy Yi

### PDF
Präsentation:
```console
cd Presentation/
xelatex Presentation.tex
xelatex Presentation.tex
```